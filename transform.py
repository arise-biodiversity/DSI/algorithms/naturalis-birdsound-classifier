# Parts of this code are taken from: https://github.com/jhartquist/fastai_audio/blob/master/fastai_audio/transform.py

import torch
import librosa as lr
import numpy as np
from config import *

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def get_frequency_transforms(n_fft=512, n_hop=int(SIGNAL_LENGTH * SAMPLE_RATE / (SPEC_SHAPE[1] - 1)), window=torch.hann_window,
                             n_mels=80, f_min=150, f_max=15000, sample_rate=48000,
                             decibels=True, ref='max', top_db=80.0, norm_db=True):
    tfms = [Spectrogram(n_fft=n_fft, n_hop=n_hop, window=window)]
    if n_mels is not None:
        tfms.append(FrequencyToMel(n_mels=n_mels, n_fft=n_fft, sr=sample_rate,
                                   f_min=f_min, f_max=f_max))
    # only one list, as its applied to all dataloaders
    return tfms


def get_frequency_batch_transforms(inputs, *args, add_channel_dim=True, **kwargs):
    
    tfms = get_frequency_transforms(*args, **kwargs)

    def _freq_batch_transformer(inputs):
        xs = inputs
        for tfm in tfms:
            xs = tfm(xs)
        #if add_channel_dim:
        #    xs.unsqueeze_(1)
        return xs
    
    return _freq_batch_transformer(inputs)


class FrequencyToMel:
    def __init__(self, n_mels=80, n_fft=512, sr=16000,
                 f_min=0.0, f_max=None, device=None):
        mel_fb = lr.filters.mel(sr=sr, n_fft=n_fft, n_mels=n_mels,
                                fmin=f_min, fmax=f_max).astype(np.float32)
        self.mel_filterbank = torch.from_numpy(mel_fb).to(device)

    def __call__(self, spec_f):
        spec_m = self.mel_filterbank @ spec_f
        return spec_m


# Returns power spectrogram (magnitude squared)
class Spectrogram:
    def __init__(self, n_fft=1024, n_hop=256, window=torch.hann_window,
                 device=None):
        self.n_fft = n_fft
        self.n_hop = n_hop
        self.window = window(n_fft).to(device)

    def __call__(self, x):
        X = torch.stft(x,
                       n_fft=self.n_fft,
                       hop_length=self.n_hop,
                       win_length=self.n_fft,
                       window=self.window,
                       onesided=True,
                       center=True,
                       pad_mode='constant',
                       normalized=True,
                       return_complex=False)
        # compute power from real and imag parts (magnitude^2)
        X.pow_(2.0)
        power = X[:,:,0] + X[:,:,1]
        return power

