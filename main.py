# -*- coding: utf-8 -*-
# @Time    : 23/02/23 15:00 
# @Author  : Burooj Ghani
# @Affiliation  : Naturalis Biodiversity Center
# @Email   : burooj.ghani at naturalis.nl
# @File    : train_pytorch.py

import argparse
import os
import torch
import torch.nn as nn
import sys
import torchvision.models as models
from torch.utils.data import Dataset, DataLoader
from dataset import Dataset
import torch.nn.functional as F
from torch.utils.data import WeightedRandomSampler
import hashlib
import gc
import time 
from root_modules import *
from models import *
from train_val import *
from util import *
from config import *
from hear21passt.base import get_basic_model, get_model_passt

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


print("I am process %s, running on %s: starting (%s)" % (
        os.getpid(), os.uname()[1], time.asctime()))

# I/O args
parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--data_dir', type=str, default='/data/burooj/ebbc2-xenocanto-chunks-3000', help='dataset directory')
parser.add_argument('--species_list', type=str, default='/data/burooj/list_gt_9', help='list of species')


# Training and optimization args
parser.add_argument('--train_batch_size', type=int, default=64, help='training batch size')
parser.add_argument('--val_batch_size', type=int, default=64, help='validation batch size')
parser.add_argument('--train_num_workers', type=int, default=60, help='number of workers during training')
parser.add_argument('--val_num_workers', type=int, default=3, help='number of workers during validation')
parser.add_argument('--max_epochs', type=int, default=30, help='maximum number of epochs')
parser.add_argument('--lr', type=float, default=0.0001, help='learning rate')
parser.add_argument('--lr_decay', type=float, default=0.1, help='learning rate decay')
parser.add_argument('--lr_decay_patience', type=int, default=5, help='learning rate decay patience')
parser.add_argument('--lr_decay_min', type=float, default=0.0001, help='minimum learning rate')
parser.add_argument('--weight_decay', type=float, default=0.0001, help='weight decay')
parser.add_argument('--momentum', type=float, default=0.9, help='momentum')
parser.add_argument('--model_name', type=str, default='passt', help='model name')
parser.add_argument('--mixup_rate', type=float, default=0.6, help='mixup rate')#0.7
parser.add_argument('--shift_prob', type=float, default=0.6, help='shift probability')#0.6
parser.add_argument('--colored_noise_prob', type=float, default=0.6, help='colored noise probability')#0.5
parser.add_argument('--background_noise_prob', type=float, default=0.6, help='background noise probability')#0.6
parser.add_argument('--epochs', type=int, default=10, help='number of epochs until the model weights for emb are initialised to zero')
parser.add_argument('--add_embedding', type=bool, default=True, help='train with BirdNet embedding')
parser.add_argument('--augmentation', type=str, default=True, help='train with augmentation')


args = parser.parse_args()



train_on_gpu=torch.cuda.is_available()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


data_dir = args.data_dir


species_list = []
with open(args.species_list) as f:
    for line in f:
        species_list.append(line.strip())

NumClasses = len(species_list)



# Parameters

params = {'batch_size': args.train_batch_size,
          'shuffle': False,
          'num_workers': args.train_num_workers}

params_v = {'batch_size': args.val_batch_size,
          'shuffle': False,
          'num_workers': args.val_num_workers}          
 


# Datasets: Create dictionaries for storing sample IDs and labels for the data loader

labels={}
IDs = []

for species in species_list:
    samples_path=os.path.join(data_dir, species)
    samples_list=os.listdir(samples_path)
    for split_sample in samples_list[:3000]:
        split_sample_path=os.path.join(samples_path, split_sample)
        labels[split_sample_path] = species
        IDs.append(split_sample_path)

partition = {}

partition['train'], partition['validation'] = split_data(IDs, test_size=0.25)
keyset=set(labels.values())
mapping = { val:key for key,val in enumerate(sorted(keyset))}


with open('/data/burooj/malta_species.txt', 'r') as f:
        allowed_species = f.read().splitlines()

allowed_indices = [key for key, value in mapping.items() if value in allowed_species]


for key in labels.keys():
        labels[key] = labels[key].replace(labels[key], str(mapping[labels[key]]))


# Compute sample weights

sample_weights, class_counts = generate_sampling_weights(labels, partition['train'])

sorted_class_counts = sorted(class_counts.items(), key=lambda x: x[1], reverse=True)

"""
with open('rank.txt', 'w') as f:    
    for item in sorted_class_counts:
        f.write(f'{item[0]}\t{item[1]}\n')
"""
# Define random sampler 
sampler = WeightedRandomSampler(weights=sample_weights,num_samples=len(partition['train']), replacement=True)

# Dataset generators

training_set = Dataset(partition['train'], labels, NumClasses, train=True, mixup_rate=args.mixup_rate, shift_prob=args.shift_prob, c_noise_prob=args.colored_noise_prob, b_noise_prob=args.background_noise_prob, species_list=species_list, embedding=args.add_embedding)
training_generator = torch.utils.data.DataLoader(training_set, sampler=sampler, **params, drop_last=True)


validation_set = Dataset(partition['validation'], labels, NumClasses, species_list=species_list, embedding=args.add_embedding)
validation_generator = torch.utils.data.DataLoader(validation_set, **params_v, drop_last=True)


# Define the model
      
if args.model_name == 'fc':
    model = FullyConnected(NumClasses)
    model.load_state_dict(torch.load('/home/ubuntu/burooj/embeddings-baseline/best_model_fc_100.pt'))

if args.model_name == 'ensemble-net':
    model_fc = FullyConnected(NumClasses)  
    model_fc.load_state_dict(torch.load('/home/ubuntu/burooj/embeddings-baseline/best_model_effnet_fc_maxpooled_Aug.pt'))
    model_DN121 =  EffNet(NumClasses, b=0)   
    model_DN121.load_state_dict(torch.load('/home/ubuntu/burooj/embeddings-baseline/best_model_effnet_maxpooled.pt'))

if args.model_name == 'ensemble-net-fc-passt':
    model_fc = FullyConnected(NumClasses)  
    model_fc.load_state_dict(torch.load('/home/ubuntu/burooj/embeddings-baseline/checkpoints/best_model_fc_438.pt'))
    model_passt = get_basic_model(mode = 'logits', arch="passt_s_kd_p16_128_ap486")
    model_passt.net =  get_model_passt(arch="passt_s_kd_p16_128_ap486",  n_classes=NumClasses)
    #model_passt.load_state_dict(torch.load('/home/ubuntu/burooj/passt_hear21/best_model_passt_439_step2.pt'))

if args.model_name == 'passt':
    model = get_basic_model(mode = 'logits', arch="passt_s_kd_p16_128_ap486")
    model.net =  get_model_passt(arch="passt_s_kd_p16_128_ap486",  n_classes=NumClasses)
    
    #model.load_state_dict(torch.load('/home/ubuntu/burooj/passt_hear21/best_model_passt_MD_439_step2.pt'))
    #model.load_state_dict(torch.load('/home/ubuntu/burooj/AvesEcho/checkpoints/best_model_passt_439_Aug_KD_3000.pt'))
    


# Define the loss function
criterion = nn.CrossEntropyLoss()


# Evaluate the ensemble model
if args.model_name == 'ensemble-net-fc-passt':
    if train_on_gpu:
        model_fc.cuda()
        model_passt.cuda()
    evaluate_ensemble_model(model_fc, model_passt, validation_generator, criterion, device, NumClasses, mapping)

else:

    if train_on_gpu:
        model.cuda()
    
    # Define the optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)

    # Define the scheduler
    #scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=args.lr_decay, patience=args.lr_decay_patience, verbose=True)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=2, gamma=0.5)

    # Train the model
    train_model(model, training_generator, validation_generator, criterion, optimizer, scheduler, device, args.max_epochs, NumClasses, sampler, training_set, params, mapping)

