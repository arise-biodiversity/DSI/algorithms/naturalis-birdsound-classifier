# AvesEcho

## Introduction

This repo contains AvesEcho v0.1 model and scripts for processing a batch of audio data.
We will keep this repository up-to-date with new models and improved interfaces to enable people to run the analysis.

## Building the model (using docker)

- Install the following:

  - [Docker](https://docs.docker.com/get-docker/).
  - [Docker compose](https://docs.docker.com/compose/install/).
  - [Git LFS](https://git-lfs.com/).

- Clone the AvesEcho repo:

  ```bash
  git clone git@gitlab.com:arise-biodiversity/DSI/algorithms/naturalis-birdsound-classifier.git
  ```

- Change directory to the repo:

  ```bash
  cd naturalis-birdsound-classifier
  ```

- Build the Docker image using:

  ```bash
  git lfs pull
  docker build -f docker/avesecho/Dockerfile -t avesecho .
  ```

  > **Note:** May require sudo priviledge. Use at your own discretion.

## Running the AvesEcho Service with Docker Compose

AvesEcho is containerized for easy deployment and execution. To run the service, you can use the provided `docker-compose.yml` file, which defines the necessary settings and parameters for the Docker container.

### Docker Compose Configuration

Below are key configurations in the `docker-compose.yml` file:

- `image: avesecho`: This specifies the name of the Docker image to use for the container.
- `command`: This defines the arguments that accompany the command that will run when the container starts up (defined in `start.sh`):
  - `--mconf`: Sets the confidence threshold for the model.
  - `--add_csv`: Enables output in CSV format in addition to the JSON output.
  - `--add_filtering`: Adds location based filtering.
  - `--flist`: a species list needs to be supplied if adding the `--add_filtering` flag.
  - `--i`: Sets the input directory for audio files within the container.
- `deploy.resources.reservations.devices`: Reserves GPU resources for the container, allowing for GPU acceleration in processing.
- `volumes`: Mounts the local directory to the container for persistent storage and access to data:
  - `/home/burooj/models/naturalis-birdsound-classifier/outputs:/app/outputs`: Mounts the host outputs directory to the container's working directory. Make sure to point it to your own host outputs directory. Note that, this can be a directory located outside the model directory on your host machine.
- `shm_size: 4g`: Allocates 4GB of shared memory to the container, which is necessary for memory-intensive operations.

### On a machine with GPU support

To start the service with Docker Compose, run the following command in the terminal from the same directory as your `docker-compose.yml`:

```bash
docker compose up
```

> **Note:** May require sudo priviledge. Use at your own discretion.

### On a machine without GPU support

```bash
docker compose -f docker-compose.yml up
```

> **Note:** May require sudo priviledge. Use at your own discretion.

On successful execution the results will be stored in `/outputs`.

## Running the AvesEcho Service with Docker

### On a machine with GPU support

To run the service without Docker Compose on a machine with GPU support, you may run the following command:

```bash
docker run --gpus all -v ./outputs:/app/outputs -v ./audio:/app/audio avesecho --i audio/
```

> **Note:** May require sudo priviledge. Use at your own discretion.

### On a machine without GPU support

To run the service without Docker Compose on a machine without GPU support, you may run the following command:

```bash
docker run -v ./outputs:/app/outputs -v ./audio:/app/audio avesecho --i audio/
```

> **Note:** May require sudo priviledge. Use at your own discretion.

### Interactive mode

To open your container in an interactive mode with a bash shell, use the following command:

```bash
docker run --gpus all -it --entrypoint /bin/bash avesecho
```

> **Note:** May require sudo priviledge. Use at your own discretion.
