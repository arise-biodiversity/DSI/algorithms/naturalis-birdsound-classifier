# -*- coding: utf-8 -*-
# @Time    : 19/10/23 15:30 
# @Author  : Burooj Ghani and Dan Stowell
# @Affiliation  : Naturalis Biodiversity Center
# @Email   : burooj.ghani at naturalis.nl
# @File    : species_list.py


import pandas as pd
import mgrs


def get_species_list(recording_lat, recording_long, df):
    "Concert WGS84 lat-lon to MGRS UTM coordinates"
    
    m = mgrs.MGRS()
    c = m.toMGRS(recording_lat, recording_long)

    input_code = c[0:5] 

    # Filter the DataFrame based on the input code
    filtered_df = df[df['cell50x50'].str.startswith(input_code)]

    # Get the list of unique bird species from the filtered DataFrame
    species_list = pd.DataFrame(filtered_df['birdlife_scientific_name'].drop_duplicates())
    
    return species_list


# Load occurances data from csv file
file_path = "inputs/ebba2_data_occurrence_50km.csv"
df = pd.read_csv(file_path, delimiter=';')


##########################
if __name__ == '__main__':
    # For demonstration purposes
    from sys import argv
    if len(argv)==3:
        recording_lat = float(argv[1])
        recording_long = float(argv[2])
    else:
        recording_lat = 52.38  # Example latitude
        recording_long = 4.64  # Example longitude
    print(f"# Species list for {recording_lat}, {recording_long}:")

    species_present = get_species_list(recording_lat, recording_long, df)

    for sp in sorted(list(species_present['birdlife_scientific_name'])):
        print(sp)

    species_present.to_csv('inputs/species_list_nl.csv', index=False, header=False)


