from config import *
import torch
import time
import torch.nn.functional as F
from models import *


# define the training function

def train(model, data_loader, criterion, optimizer, device, train_metrics, epoch):
    model.train()
    torch.set_grad_enabled(True)
    running_loss = 0.0
    t=1.0
    l=0.5
    
    #running_corrects = 0
    all_predictions = torch.tensor([], dtype=torch.float32, device=device)
    all_labels = torch.tensor([], dtype=torch.float32, device=device)

    #load teacher model

    
    model_teacher = FullyConnected(439)
    #model_teacher.load_state_dict(torch.load('/home/ubuntu/burooj/embeddings-baseline/checkpoints/best_model_fc_438.pt')) #(previous best)
    model_teacher.load_state_dict(torch.load('/home/ubuntu/burooj/AvesEcho/checkpoints/best_model_fc_3000.pt'))
    model_teacher.cuda()
    model_teacher.eval()

    total_batches = len(data_loader)

    for i, (inputs, labels) in enumerate(data_loader):
        #start_time = time.time()
        images = inputs['inputs']
        #logits = inputs['targets']
        #logits = logits.to(device)
        emb = inputs['emb']
        images = images.to(device)
        emb = emb.to(device)
       
        
        # convert the integer labels into one-hot encoding format
        #labels = torch.nn.functional.one_hot(labels, num_classes=NumClasses).float()
        labels = labels.to(device)
       

        optimizer.zero_grad()
        outputs = model(images)

        
        
        #####loss_g = criterion(F.softmax(outputs, dim=1), labels)

        loss_g = criterion(outputs, labels)

        
        logits = model_teacher(images, emb)
        # We need to apply log_softmax to our tensors before using them with kl_div.
        p = F.softmax(outputs, dim=1)
        log_q = F.log_softmax(logits/t, dim=1)

        kld_loss = F.kl_div(log_q, p, reduction='batchmean')
        

        loss = l*loss_g + (1-l)*kld_loss
        
        #loss = loss_g      
        
        loss.backward()
        
        optimizer.step()
        running_loss += loss.item()
        average_loss = running_loss / (i+1)
        
        #transform back labels from one-hot encoding to integer labels
        #labels = torch.argmax(labels)


    # once loss has been computed on one-hot encoded labels, convert back to integer labels for metrics
    # (loss computed on labels of type, for instance, 0 0.7 0.3 0 (4 classes) will be different than loss computed on labels of type 0 1 0 0 (4 classes), you see?)
        _, predicted = torch.max(outputs, 1)
        labels = torch.argmax(labels, axis=1)

        all_predictions = torch.cat((all_predictions, predicted), 0)
        all_labels = torch.cat((all_labels, labels), 0)

        #print(all_predictions.shape)
        #print(all_labels.shape)

        output = train_metrics(all_predictions, all_labels)

        
        
        # Compute precision, recall, F1 score using PyTorch's built-in functions
       
        #metric = AUROC(task='multiclass',num_classes=NumClasses, average="macro", thresholds=None).to(device)
        auc = 0



        #end_time = time.time()
        
        #print(f"Elapsed_time: {end_time - start_time}")
    
        print('\r{}/{} Training Loss: {:.4f} Acc: {:.4f} F1: {:.4f} P: {:.4f} R: {:.4f} AUC: {:.4f}'.format(i, total_batches, average_loss, output['train_MulticlassAccuracy'].item(), output['train_MulticlassF1Score'].item(), output['train_MulticlassPrecision'].item(), output['train_MulticlassRecall'].item(), auc), end='')
        #if i == 0:
        #    break

    return running_loss / len(data_loader)

    #end_time = time.time()
    #print(f"Elapsed_time: {end_time - start_time}")

# define the vadiation function
def validate(model, data_loader, criterion, device, val_metrics, NumClasses, mapping, epoch):
    model.eval()
    torch.set_grad_enabled(False)
    running_loss = 0.0
    #running_corrects = 0
    all_predictions = torch.tensor([], dtype=torch.float, device=device)
    all_labels = torch.tensor([], dtype=torch.float, device=device)
    all_outputs = torch.tensor([], dtype=torch.float, device=device)
    mpooled_outputs = torch.tensor([], dtype=torch.float32, device=device)
    mpooled_labels = torch.tensor([], dtype=torch.float32, device=device)
    #predict = torch.zeros(data_loader.batch_size).to(torch.int).to(device)
    
    all_samples = []
    class_wise_metric = ClasswiseWrapper(MulticlassAccuracy(num_classes=439, average=None), labels=list(mapping.keys())).to(device)
    

    for i, (inputs, labels) in enumerate(data_loader):

        images = inputs['inputs']
        #emb = inputs['emb']
        samples = inputs['samples']

        all_samples.extend(samples)
        

        images = images.to(device)
        #images = F.interpolate(images, size=(224, 224), mode='bilinear', align_corners=False)
        #images = images.repeat(1, 3, 1, 1)
        
        labels = labels.to(device)
        #emb = emb.to(device)
        outputs = model(images)
        loss = criterion(outputs, labels)
        running_loss += loss.item()

        _, predicted = torch.max(outputs, 1)
        labels = torch.argmax(labels, axis=1)


        all_predictions = torch.cat((all_predictions, predicted), 0)
        all_labels = torch.cat((all_labels, labels), 0)
        all_outputs = torch.cat((all_outputs, outputs), 0)

        '''image_indices_dict = {}
        for i, sample_path in enumerate(samples):
            image_name = os.path.splitext(os.path.basename(sample_path))[0].split('_')[0]
            if image_name not in image_indices_dict:
                image_indices_dict[image_name] = []
            image_indices_dict[image_name].append(i)

        
        for image_name, indices in image_indices_dict.items():
            max_score_index = max(indices, key=lambda i: outputs[i].max())
            mean_pool = torch.mean(outputs[indices], dim=0)
            max_pool = torch.max(outputs[indices], dim=0)[0]

            for i in indices:
                predict[i] = torch.argmax(outputs[max_score_index])
                outputs[i] = mean_pool #max_pool'''


        # Compute precision, recall, F1 score using PyTorch's built-in functions
       
        metric = AUROC(task='multiclass',num_classes=NumClasses, average="macro", thresholds=None).to(device)
        auc = metric(outputs, labels)
       

    image_indices_dict = {}
    predict = torch.zeros(len(all_samples)).to(torch.int).to(device)

    for i, sample_path in enumerate(all_samples):
        image_name = os.path.splitext(os.path.basename(sample_path))[0].split('_')[0]
        if image_name not in image_indices_dict:
            image_indices_dict[image_name] = []
        image_indices_dict[image_name].append(i)

    for image_name, indices in image_indices_dict.items():
            #max_score_index = max(indices, key=lambda i: all_outputs[i].max())
            mean_pool = torch.mean(all_outputs[indices], dim=0)
            max_pool = torch.max(all_outputs[indices], dim=0)[0]

            mpooled_outputs = torch.cat((mpooled_outputs, max_pool.unsqueeze(0)), 0)
            mpooled_labels = torch.cat((mpooled_labels, all_labels[indices[0]].unsqueeze(0)), 0)

            for i in indices:
                #predict[i] = torch.argmax(all_outputs[max_score_index])
                all_outputs[i] = max_pool #mean_pool  

    #output_maxpooled = val_metrics(predict, all_labels)               

    #output = output_maxpooled

    #output = val_metrics(all_predictions, all_labels)
    #print(output['val_MulticlassConfusionMatrix'])
    #class_wise_output = class_wise_metric(all_predictions, all_labels)

    #class_wise_output = class_wise_metric(predict, all_labels)

    #for key in class_wise_output:
    #    class_wise_output[key] = class_wise_output[key].item()

    # accuracy
    metrics_n_k = torchmetrics.MetricCollection([MulticlassAccuracy(num_classes=NumClasses, top_k=1)])
    val_metrics_n_k = metrics_n_k.clone(prefix='val_').to(device)
    class_wise_metric_n_k = ClasswiseWrapper(MulticlassAccuracy(num_classes=NumClasses, average=None, top_k=1), labels=list(mapping.keys())).to(device)
    output_n_k = val_metrics_n_k(all_outputs, all_labels.long())
    output_n_k_mpooled = val_metrics_n_k(mpooled_outputs, mpooled_labels.long())
    class_wise_output_n_k = class_wise_metric_n_k(mpooled_outputs, mpooled_labels.long())

    # f1-score
    metrics_n_k_f1 = torchmetrics.MetricCollection([MulticlassF1Score(num_classes=NumClasses, top_k=1)])
    val_metrics_n_k_f1 = metrics_n_k_f1.clone(prefix='val_').to(device)
    output_n_k_mpooled_f1 = val_metrics_n_k_f1(mpooled_outputs, mpooled_labels.long())

    # confusion matrix
    metrics_n_k_CM = torchmetrics.MetricCollection([MulticlassConfusionMatrix(num_classes=NumClasses, top_k=1)])
    val_metrics_n_k_CM = metrics_n_k_CM.clone(prefix='val_').to(device)
    output_n_k_CM = val_metrics_n_k_CM(all_outputs, all_labels.long())
    output_n_k_CM_mpooled = val_metrics_n_k_CM(mpooled_outputs, mpooled_labels.long())

    #metrics_n_k = torchmetrics.MetricCollection([MulticlassAccuracy(num_classes=NumClasses, top_k=5)])
    #val_metrics_n_k = metrics_n_k.clone(prefix='val_').to(device)
    #output_n_k = val_metrics_n_k(all_outputs, all_labels.long())
    #print(output_n_k['val_MulticlassAccuracy'])

    for key in class_wise_output_n_k:
        class_wise_output_n_k[key] = class_wise_output_n_k[key].item()


    #np.save('scores_effNet_SansEmb_maxpooled.npy', class_wise_output)
    #np.save('scores_passt_439_step2_SpecAug.npy', class_wise_output_n_k)
    np.save('/home/ubuntu/burooj/AvesEcho/outputs/scores_passt_439_Aug_3000_KD_48k.npy', class_wise_output_n_k)
    #np.save('confusion_matrix_effNet_SansEmb_maxpooled.npy', output['val_MulticlassConfusionMatrix'].cpu()) 
    #np.save('confusion_matrix_passt_439_step2_SpecAug.npy', output_n_k_CM_mpooled['val_MulticlassConfusionMatrix'].cpu())   
    np.save('/home/ubuntu/burooj/AvesEcho/outputs/confusion_matrix_passt_439_Aug_KD_3000_48k.npy', output_n_k_CM_mpooled['val_MulticlassConfusionMatrix'].cpu()) 
          

    return running_loss / len(data_loader), output_n_k_mpooled['val_MulticlassAccuracy'].item(), output_n_k_mpooled_f1['val_MulticlassF1Score'].item(),
    #return running_loss / len(data_loader), output['val_MulticlassAccuracy'].item(), output['val_MulticlassF1Score'].item(), output['val_MulticlassPrecision'].item(), output['val_MulticlassRecall'].item(), auc

# Define the train model method 
def train_model(model, train_loader, val_loader, criterion, optimizer, scheduler, device, num_epochs, NumClasses, sampler, training_set, params, mapping):
    best_loss = np.inf
    metrics = torchmetrics.MetricCollection([
            # Accuracy: due to mode multiclass, not multilabel, this uses same formula as Precision
            MulticlassAccuracy(num_classes=NumClasses),
            MulticlassPrecision(num_classes=NumClasses),
            MulticlassRecall(num_classes=NumClasses),
            MulticlassF1Score(num_classes=NumClasses),
            MulticlassConfusionMatrix(num_classes=NumClasses)
        ])


    train_metrics = metrics.clone(prefix='train_').to(device)
    valid_metrics = metrics.clone(prefix='val_').to(device)

    for epoch in range(num_epochs):
        train_loss = 0.0

        #training_loader = torch.utils.data.DataLoader(training_set, sampler=sampler, **params)

        """ 'Draws a multinomial distribution over the training set and allows for replacement'
        indices = np.arange(len(partition['train']))
        random_indices = np.random.choice(indices, size=len(indices), p=sample_weights/np.sum(sample_weights))
        train_set_multinomial = [partition['train'][i] for i in random_indices]
        training_set = Dataset(train_set_multinomial, labels)
        train_loader = torch.utils.data.DataLoader(training_set, **params) """

        train_loss = train(model, train_loader, criterion, optimizer, device, train_metrics, epoch)
        val_loss = validate(model, val_loader, criterion, device, valid_metrics, NumClasses, mapping, epoch)
        #scheduler.step(val_loss[0])
        scheduler.step()
        if val_loss[0] < best_loss:
            best_loss = val_loss[0]
            #torch.save(model.state_dict(), 'best_model_passt_439_step2_SpecAug.pt')
            torch.save(model.state_dict(), '/home/ubuntu/burooj/AvesEcho/checkpoints/best_model_passt_439_Aug_KD_3000_48k.pt')
        print(f'Epoch: {epoch+1}, Train Loss: {train_loss:.4f}, Val Loss: {val_loss[0]:.4f}, Best Loss: {best_loss:.4f}, v_Accuracy: {val_loss[1]:.4f}, v_f1score: {val_loss[2]:.4f}')    
        #print(f'Epoch: {epoch+1}, Train Loss: {train_loss:.4f}, Val Loss: {val_loss[0]:.4f}, Best Loss: {best_loss:.4f}, v_Accuracy: {val_loss[1]:.4f}, v_F1: {val_loss[2]:.4f}, v_Precision: {val_loss[3]:.4f}, v_Recall: {val_loss[4]:.4f}, v_AUC: {val_loss[5]:.4f}')

def BirdNet_Preds(train_loader, val_loader, device, num_epochs, NumClasses, sampler, training_set, params, mapping):
   
    class_wise_metric = ClasswiseWrapper(MulticlassAccuracy(num_classes=439, average=None), labels=list(mapping.keys())).to(device)
    all_predictions = torch.tensor([], dtype=torch.float, device=device)
    all_labels = torch.tensor([], dtype=torch.float, device=device)
    metrics = torchmetrics.MetricCollection([
            # Accuracy: due to mode multiclass, not multilabel, this uses same formula as Precision
            MulticlassAccuracy(num_classes=NumClasses),
            MulticlassPrecision(num_classes=NumClasses),
            MulticlassRecall(num_classes=NumClasses),
            MulticlassF1Score(num_classes=NumClasses),
            MulticlassConfusionMatrix(num_classes=NumClasses)
        ])
    valid_metrics = metrics.clone(prefix='val_').to(device)
    total_batches = len(val_loader)

    

    for j, (inputs, labels) in enumerate(val_loader):

        #images = inputs['inputs']
        #emb = inputs['emb']
        preds = inputs['labels']
        samples = inputs['samples']
        labels = labels.to(device)
        labels = torch.argmax(labels, axis=1)



        # Read the CSV file into a pandas DataFrame
        df = pd.read_csv('/home/ubuntu/burooj/embeddings-baseline/output_species.csv')



        # Loop through the list of species names
        for i in range(len(preds)):
            # Add a space before every uppercase letter (except the first letter)
            #modified_name = ''.join([' '+c if c.isupper() and i!=0 else c for i,c in enumerate(preds[i])]).lstrip()
            # Add a space before every uppercase letter (except the first letter and uppercase letters preceded by a hyphen)
            modified_name = ''.join([' ' + c if c.isupper() and j != 0 and preds[i][j - 1] != '-' else c for j, c in enumerate(preds[i])]).lstrip()
            # Look up the part after underscore in the second column of the DataFrame
            match = df[df['ebird'].apply(lambda x: x.split('_')[1])==modified_name]
            # If there is a match, extract the part after underscore in the first column and replace the element in the list
            if not match.empty:
                code = match.iloc[0]['XC'].split('_')[1].replace(' ','').replace('\'','')
                preds[i] = code
        

        preds = [mapping.get(k, mapping['noise']) for k in preds]
        preds = torch.tensor(preds).to(device)

        all_predictions = torch.cat((all_predictions, preds), 0)
        all_labels = torch.cat((all_labels, labels), 0)

        
        output = valid_metrics(all_predictions, all_labels) 
        
        #print(output['val_MulticlassConfusionMatrix']) 

        print('\r{}/{} Acc: {:.4f} F1: {:.4f} P: {:.4f} R: {:.4f}'.format(j, total_batches, output['val_MulticlassAccuracy'].item(), output['val_MulticlassF1Score'].item(), output['val_MulticlassPrecision'].item(), output['val_MulticlassRecall'].item()), end='') 

    class_wise_output = class_wise_metric(all_predictions, all_labels)

    for key in class_wise_output:
        class_wise_output[key] = class_wise_output[key].item()

    #np.save('scores_birdnet_no_selection_table_new.npy', class_wise_output)
    np.save('confusion_matrix_no_selection_table_new.npy', output['val_MulticlassConfusionMatrix'].cpu())
    
    return output['val_MulticlassAccuracy'].item(), output['val_MulticlassF1Score'].item(), output['val_MulticlassPrecision'].item(), output['val_MulticlassRecall'].item()
    
 
    #print(f'Epoch: {epoch+1}, Train Loss: {train_loss:.4f}, Val Loss: {val_loss[0]:.4f}, Best Loss: {best_loss:.4f}, v_Accuracy: {val_loss[1]:.4f}, v_F1: {val_loss[2]:.4f}, v_Precision: {val_loss[3]:.4f}, v_Recall: {val_loss[4]:.4f}, v_AUC: {val_loss[5]:.4f}')

def evaluate_ensemble_model(model1, model2, data_loader, criterion, device, NumClasses, mapping):
   
    metrics = torchmetrics.MetricCollection([
            # Accuracy: due to mode multiclass, not multilabel, this uses same formula as Precision
            MulticlassAccuracy(num_classes=NumClasses),
            MulticlassPrecision(num_classes=NumClasses),
            MulticlassRecall(num_classes=NumClasses),
            MulticlassF1Score(num_classes=NumClasses),
            MulticlassConfusionMatrix(num_classes=NumClasses)
        ])


    
    valid_metrics = metrics.clone(prefix='val_').to(device)
    model1.eval()
    model2.eval()
    torch.set_grad_enabled(False)
    running_loss = 0.0
    #running_corrects = 0
    all_predictions = torch.tensor([], dtype=torch.float, device=device)
    all_labels = torch.tensor([], dtype=torch.float, device=device)
    all_outputs = torch.tensor([], dtype=torch.float, device=device)
    mpooled_outputs = torch.tensor([], dtype=torch.float32, device=device)
    mpooled_labels = torch.tensor([], dtype=torch.float32, device=device)
    #all_outputs_mean = torch.tensor([], dtype=torch.float, device=device) 
    epoch = 1
    #predict = torch.zeros(data_loader.batch_size).to(torch.int).to(device)
    
    all_samples = []
    class_wise_metric = ClasswiseWrapper(MulticlassAccuracy(num_classes=439, average=None), labels=list(mapping.keys())).to(device)
    
     
    for i, (inputs, labels) in enumerate(data_loader):

        images = inputs['inputs']
        emb = inputs['emb']
        samples = inputs['samples']

        all_samples.extend(samples)
        

        images = images.to(device)
        #images = F.interpolate(images, size=(224, 224), mode='bilinear', align_corners=False)
        #images = images.repeat(1, 3, 1, 1)
        
        labels = labels.to(device)
        emb = emb.to(device)
        outputs1 = model1(images, emb)
        outputs2 = model2(images)
        #outputs = (outputs1 + outputs2) / 2
        outputs = torch.max(F.softmax(outputs1, dim=1), F.softmax(outputs2, dim=1))
        #loss1 = criterion(outputs1, labels)
        #loss2 = criterion(outputs2, labels)
        #running_loss += loss1.item()
        #running_loss2 += loss2.item()


        _, predicted = torch.max(outputs, 1)
        labels = torch.argmax(labels, axis=1)


        all_predictions = torch.cat((all_predictions, predicted), 0)
        all_labels = torch.cat((all_labels, labels), 0)
        all_outputs = torch.cat((all_outputs, outputs), 0)

        '''image_indices_dict = {}
        for i, sample_path in enumerate(samples):
            image_name = os.path.splitext(os.path.basename(sample_path))[0].split('_')[0]
            if image_name not in image_indices_dict:
                image_indices_dict[image_name] = []
            image_indices_dict[image_name].append(i)

        
        for image_name, indices in image_indices_dict.items():
            max_score_index = max(indices, key=lambda i: outputs[i].max())
            mean_pool = torch.mean(outputs[indices], dim=0)
            max_pool = torch.max(outputs[indices], dim=0)[0]

            for i in indices:
                predict[i] = torch.argmax(outputs[max_score_index])
                outputs[i] = mean_pool #max_pool'''


        # Compute precision, recall, F1 score using PyTorch's built-in functions
       
        metric = AUROC(task='multiclass',num_classes=NumClasses, average="macro", thresholds=None).to(device)
        #auc = metric(outputs, labels)
       






    image_indices_dict = {}
    all_outputs_mean = all_outputs.clone()
    predict = torch.zeros(len(all_samples)).to(torch.int).to(device)

    for i, sample_path in enumerate(all_samples):
        image_name = os.path.splitext(os.path.basename(sample_path))[0].split('_')[0]
        if image_name not in image_indices_dict:
            image_indices_dict[image_name] = []
        image_indices_dict[image_name].append(i)

    for image_name, indices in image_indices_dict.items():
            #max_score_index = max(indices, key=lambda i: all_outputs[i].max())
            mean_pool = torch.mean(all_outputs[indices], dim=0)
            max_pool = torch.max(all_outputs[indices], dim=0)[0]

            mpooled_outputs = torch.cat((mpooled_outputs, max_pool.unsqueeze(0)), 0)
            mpooled_labels = torch.cat((mpooled_labels, all_labels[indices[0]].unsqueeze(0)), 0)            

            for i in indices:
                #predict[i] = torch.argmax(all_outputs[max_score_index])
                all_outputs[i] = max_pool #mean_pool  
                all_outputs_mean[i] = mean_pool

    #output_maxpooled = val_metrics(predict, all_labels)               

    #output = output_maxpooled

    #output = val_metrics(all_predictions, all_labels)
    #print(output['val_MulticlassConfusionMatrix'])
    #class_wise_output = class_wise_metric(all_predictions, all_labels)

    #class_wise_output = class_wise_metric(predict, all_labels)

    #for key in class_wise_output:
    #    class_wise_output[key] = class_wise_output[key].item()




    #Compute top 5 accuracy
    
    metrics_n_k = torchmetrics.MetricCollection([MulticlassAccuracy(num_classes=NumClasses, top_k=1)])
    val_metrics_n_k = metrics_n_k.clone(prefix='val_').to(device)
    class_wise_metric_n_k = ClasswiseWrapper(MulticlassAccuracy(num_classes=NumClasses, average=None, top_k=1), labels=list(mapping.keys())).to(device)
    output_n_k = val_metrics_n_k(all_outputs, all_labels.long())
    output_n_k_mpooled = val_metrics_n_k(mpooled_outputs, mpooled_labels.long())
    class_wise_output_n_k_mpooled = class_wise_metric_n_k(mpooled_outputs, mpooled_labels.long())
    class_wise_output_n_k = class_wise_metric_n_k(all_outputs, all_labels.long())
    print(output_n_k_mpooled['val_MulticlassAccuracy'])
    
    metrics_n_k_CM = torchmetrics.MetricCollection([MulticlassAccuracy(num_classes=NumClasses, top_k=5)])
    val_metrics_n_k = metrics_n_k.clone(prefix='val_').to(device)
    output_n_k = val_metrics_n_k(all_outputs, all_labels.long())
    output_n_k_mpooled = val_metrics_n_k(mpooled_outputs, mpooled_labels.long())
    print(output_n_k_mpooled['val_MulticlassAccuracy']) 

    metrics_n_k_CM = torchmetrics.MetricCollection([MulticlassConfusionMatrix(num_classes=NumClasses, top_k=1)])
    val_metrics_n_k_CM = metrics_n_k_CM.clone(prefix='val_').to(device)
    output_n_k_CM = val_metrics_n_k_CM(all_outputs, all_labels.long())
    output_n_k_CM_mpooled = val_metrics_n_k_CM(mpooled_outputs, mpooled_labels.long())
    

    for key in class_wise_output_n_k:
        class_wise_output_n_k[key] = class_wise_output_n_k[key].item()

    #print(output_n_k['val_MulticlassAccuracy'])
    #np.save('scores_effNet_SansEmb_maxpooled.npy', class_wise_output)
    #np.save('scores_effNet_SansEmb_maxpooled.npy', class_wise_output_n_k)
    #np.save('confusion_matrix_effNet_SansEmb_maxpooled.npy', output['val_MulticlassConfusionMatrix'].cpu())    
          

    #return running_loss / len(data_loader), output_n_k['val_MulticlassAccuracy'].item()
    #return running_loss / len(data_loader), output['val_MulticlassAccuracy'].item(), output['val_MulticlassF1Score'].item(), output['val_MulticlassPrecision'].item(), output['val_MulticlassRecall'].item(), auc



def inference(model, data_loader, device, mapping, flist):
    model.eval()  # Set the model to evaluation mode
    torch.set_grad_enabled(False)  # Disable gradient calculation
    all_predictions = []
    all_scores = []
    all_predictions_filtered = []  # Store all predictions
    all_scores_filtered = []
    all_images = []
    all_filtered_outputs = torch.Tensor()
    all_filtered_outputs = all_filtered_outputs.to(device)

    epoch = 1

    with open(flist, 'r') as f:
        allowed_species = f.read().splitlines()

    #allowed_indices = [value for key, value in mapping.items() if key in allowed_species]
    allowed_indices = [mapping.index(species) for species in allowed_species if species in mapping]
    allowed_species_names = [species for species in allowed_species if species in mapping]


    

    for i, inputs in enumerate(data_loader):  # Ignore labels
        images = inputs['inputs']
        emb = inputs['emb']
        images = images.to(device)
        emb = emb.to(device)
        files = inputs['file']
        # PaSST
        outputs = model(images)  # Forward pass
        # FC 
        #outputs = model(images, emb)  # Forward pass
        outputs = F.softmax(outputs, dim=1)

        # Filter the outputs to only consider allowed species
        filtered_outputs = outputs[:, allowed_indices]

        scores, predicted = torch.max(outputs, 1)  # Get predicted class

        scores_filtered, predicted_filtered = torch.max(filtered_outputs, 1)  # Get predicted class from filtered outputs

        # Convert the predicted indices back to original indices
        predicted_original_indices = [allowed_indices[i] for i in predicted_filtered.tolist()]


        
        predictions = [mapping[i] for i in predicted.tolist()]
        predicted_filtered = [mapping[i] for i in predicted_original_indices]


        all_predictions.extend(predictions)
        all_scores.extend(scores.tolist())
        all_predictions_filtered.extend(predicted_filtered)
        all_scores_filtered.extend(scores_filtered.tolist())
        all_images.extend(files)
        all_filtered_outputs = torch.cat((all_filtered_outputs, filtered_outputs), 0)

    #return all_predictions, all_predictions_filtered, all_scores_filtered, all_scores, all_images, all_filtered_outputs, allowed_species_names
    return all_predictions, all_predictions_filtered, all_scores_filtered, all_scores, all_images