#!/bin/bash

# Directory containing wav files
DATA_DIR="/data/burooj/audio"

# Loop over each .wav file in the directory
for wav_file in "$DATA_DIR"/*.WAV; do
    # Ensure the loop doesn't just run with the pattern if no .wav files are found
    if [[ -f $wav_file ]]; then
        # Run the Python script with the specified arguments
        python3 analyse.py --add_csv --add_filtering --i "$wav_file"
        echo "$wav_file"
    fi
done
