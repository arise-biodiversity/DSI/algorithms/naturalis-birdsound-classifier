from __future__ import division
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # or any {'0', '1', '2'}

import warnings
warnings.filterwarnings(action='ignore')
import subprocess
import pandas as pd
import librosa
import numpy as np
import hashlib
import torchmetrics
import torch
from sklearn.metrics import roc_auc_score
from torchmetrics.classification import MulticlassF1Score
from torchmetrics.classification import MulticlassAccuracy
from torchmetrics.classification import MulticlassPrecision
from torchmetrics.classification import MulticlassRecall
from torchmetrics.classification import MulticlassAUROC
from torchmetrics.classification import AUROC
from torchmetrics.classification import MulticlassConfusionMatrix
from torchmetrics import ClasswiseWrapper
import logging
from datetime import datetime

from sklearn.utils import shuffle
from PIL import Image
from tqdm import tqdm
import matplotlib.pyplot as plt

import tensorflow as tf
from sklearn.model_selection import train_test_split
import keras
import pickle
import tensorflow_addons as tfa
from tensorflow import keras
from keras import layers
from keras.callbacks import ModelCheckpoint
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score, classification_report, confusion_matrix



train_on_gpu=torch.cuda.is_available()
#print("train_on_gpu: ", train_on_gpu)
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


# Global vars
SEED = 78

torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

RANDOM_SEED = 1337
SAMPLE_RATE_AST = 32000
SAMPLE_RATE = 48000
SIGNAL_LENGTH = 3 # seconds
SPEC_SHAPE = (298, 128) # width x height
FMIN = 20
FMAX = 15000
MAX_AUDIO_FILES = 10000



