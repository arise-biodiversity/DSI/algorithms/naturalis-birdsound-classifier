import os
import sys
import torchaudio.transforms


#module_dir = "/home/ubuntu/burooj/BirdNET-Analyzer"
#sys.path.append(module_dir)


#from embeddings_to_vector import analyzeChunk
import torch
import torchaudio.transforms as T
import numpy as np
from PIL import Image
import torchvision.transforms as transforms
from config import *
from transform import *
from util import *
import time
from torch_audiomentations import AddColoredNoise, ApplyImpulseResponse, Shift, AddBackgroundNoise

os.environ["CUDA_VISIBLE_DEVICES"] = "0"

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'





class Dataset(torch.utils.data.Dataset):
    def __init__(self, list_IDs, labels, num_classes, species_list, mixup_rate=0.75, train=False, shift_prob=0.5, c_noise_prob=0.5, b_noise_prob=0.5, embedding=True):
        
        self.labels = labels
        self.list_IDs = list_IDs
        self.mixup_rate = mixup_rate
        self.num_classes = num_classes
        self.train = train
        self.embeddings = True
        #self.freqm = 48
        #self.timem = 192
        # Load channel_means and channel_stds from disk and convert them back to tensors
        self.channel_means = torch.from_numpy(np.load('/home/ubuntu/burooj/AST/channel_means_ast.npy'))
        self.channel_stds = torch.from_numpy(np.load('/home/ubuntu/burooj/AST/channel_stds_ast.npy'))
        self.noise = AddColoredNoise(p=c_noise_prob, p_mode="per_example", mode="per_example", sample_rate=SAMPLE_RATE,
                                     min_snr_in_db=25, max_snr_in_db=40, min_f_decay=-2, max_f_decay=1.5)
        self.shift = Shift(p=shift_prob, p_mode="per_example", mode="per_example", sample_rate=SAMPLE_RATE)
        self.background_noise = AddBackgroundNoise(background_paths="/data/burooj/ebbc2-xenocanto-chunks-3000/noise", min_snr_in_db=3.0, max_snr_in_db=30.0, p=b_noise_prob, sample_rate=SAMPLE_RATE)
        self.embedding_model = BirdNet(48000, '/home/ubuntu/burooj/AvesEcho/checkpoints/BirdNET_GLOBAL_3K_V2.2_Model_FP32.tflite')
        #self.embedding_model = BirdNet(48000, '/data/burooj/BirdNET-Analyzer/checkpoints/V2.4/BirdNET_GLOBAL_6K_V2.4_Model_FP32.tflite')
        self.species_list = np.array(sorted(species_list))
        self.embedding = embedding
        self.spec_aug = torch.nn.Sequential(
            torchaudio.transforms.TimeStretch(0.6, fixed_rate=True),
            torchaudio.transforms.FrequencyMasking(freq_mask_param=48),
            torchaudio.transforms.TimeMasking(time_mask_param=56),
        )

        
        if self.train:

            with open('/home/ubuntu/burooj/AvesEcho/checkpoints/labels_new.txt', 'r') as file:
                lines = file.readlines()

            self.targets = [line.strip() for line in lines]
            self.birdnet_array = np.array(self.targets)
            self.species_list = self.replace_XC_Birdnet(self.species_list)
            self.indices = np.array([np.where(self.birdnet_array == element)[0][0] if element in self.birdnet_array else -1 for element in self.species_list])
            self.indices = self.indices[self.indices != -1]


        
        
    def __len__(self):
        return len(self.list_IDs)

    def mixup_data(self, x_path, y_path, alpha=10):
        '''Returns mixed inputs, pairs of targets, and lambda'''
        
        
        specie_x, sample_x = x_path.split('/')[-2:]
        base_name_x, file_extension = os.path.splitext(sample_x)

        specie_y, sample_y = y_path.split('/')[-2:]
        base_name_y, file_extension = os.path.splitext(sample_y)

        audio_x_path = os.path.join("/data/burooj/ebbc2-xenocanto-chunks-3000", specie_x, base_name_x + '.wav')
        audio_y_path = os.path.join("/data/burooj/ebbc2-xenocanto-chunks-3000", specie_y, base_name_y + '.wav')

        # Open the file with librosa (limited to the first certain number of seconds)
        try:
            x, rate = librosa.load(audio_x_path, sr=SAMPLE_RATE, offset=0.0, res_type='kaiser_fast')
        except:
            x, rate = [], SAMPLE_RATE


        try:
            y, rate = librosa.load(audio_y_path, sr=SAMPLE_RATE, offset=0.0, res_type='kaiser_fast')
        except:
            y, rate = [], SAMPLE_RATE

        x = x - x.mean()
        y = y - y.mean()
       

        # sample lambda from beta distribtion
        lam = np.random.beta(alpha, alpha)  

        #mixup
        mixed = lam * x + (1 - lam) * y

        y1 = torch.tensor(int(self.labels[x_path]), dtype=torch.long)
        y2 = torch.tensor(int(self.labels[y_path]), dtype=torch.long)
        y1 = torch.nn.functional.one_hot(y1, self.num_classes).float()
        y2 = torch.nn.functional.one_hot(y2, self.num_classes).float()
        labels = lam * y1 + (1 - lam) * y2
        
        #transform labels back from one hot encoding
        #labels = torch.argmax(labels)


        #mixed = mixed - mixed.mean()
       
        # Compute BirdNET embedding
        #emb = analyzeChunk(mixed)

        return mixed, labels


    
    def __getitem__(self, index):

        # Mixup, only for training data

        if np.random.rand() < self.mixup_rate and self.train:
            # Keep track of the time
            #start_time = time.time()
            index2 = np.random.randint(0, len(self.list_IDs))
            sample_path = self.list_IDs[index]
            sample_path2 = self.list_IDs[index2]

            try:
                mixed, y = self.mixup_data(sample_path, sample_path2)
                #birdnet_embedding = np.reshape(emb, [emb.shape[1],])
            except:
                print("Mixup failed")
                y = torch.tensor(int(self.labels[sample_path]), dtype=torch.long)
                mixed = np.zeros(SIGNAL_LENGTH)
                #birdnet_embedding = np.zeros(320)

            #convert mixed to tensor
            mixed = torch.from_numpy(mixed).float()

            # Add waveform augmentations here from torch_audiomentations
            mixed = self.noise(mixed.unsqueeze(0).unsqueeze(0)).squeeze(0).squeeze(0)
            mixed = self.shift(mixed.unsqueeze(0).unsqueeze(0)).squeeze(0).squeeze(0)
            mixed = self.background_noise(mixed.unsqueeze(0).unsqueeze(0)).squeeze(0).squeeze(0)

            

            # Compute BirdNET embedding
            if self.train and self.embeddings:

                try:
                    embedding,logits = embed_sample(self.embedding_model, mixed.numpy(),SAMPLE_RATE)
                    #birdnet_embedding = np.expand_dims(outputs, axis=0)
                except:
                    print("BirdNET embedding failed")
                    birdnet_embedding = np.zeros(320)

            #Resample the audio from 48k to 16k for AST

            resampler = T.Resample(SAMPLE_RATE, SAMPLE_RATE_AST, dtype=mixed.dtype)
            x = resampler(mixed)
            
            

            y = torch.tensor(int(self.labels[sample_path]), dtype=torch.long)
            y = torch.nn.functional.one_hot(y, self.num_classes).float()
            

            

        else:
            #start_time = time.time()
            sample_path = self.list_IDs[index]
            specie, sample = sample_path.split('/')[-2:]
            base_name, file_extension = os.path.splitext(sample)
           
            audio_path = os.path.join("/data/burooj/ebbc2-xenocanto-chunks-3000", specie, base_name + '.wav') 
            
            # Open the file with librosa (limited to the first certain number of seconds)
            try:
                x, rate = librosa.load(audio_path, sr=SAMPLE_RATE, offset=0.0, res_type='kaiser_fast')
            except:
                x, rate = [], SAMPLE_RATE
            #print(x) 
            # 
            # 
            '''
            noise_path = '/data/burooj/ebbc2-xenocanto-chunks-3000/noise/ffeaa1c4-f880-47db-b58e_2.wav'

            # Open the file with librosa (limited to the first certain number of seconds)
            try:
                y, rate = librosa.load(noise_path, sr=SAMPLE_RATE, offset=0.0, res_type='kaiser_fast')
            except:
                y, rate = [], SAMPLE_RATE   
            
            
            x =  x + y*0.4

            '''
                           
            x = x - x.mean()

            #convert mixed to tensor
            x = torch.from_numpy(x).float()
            
            
            if self.train:
                # Add waveform augmentations here from torch_audiomentations
                x = self.noise(x.unsqueeze(0).unsqueeze(0)).squeeze(0).squeeze(0)
                x = self.shift(x.unsqueeze(0).unsqueeze(0)).squeeze(0).squeeze(0)
                x = self.background_noise(x.unsqueeze(0).unsqueeze(0)).squeeze(0).squeeze(0)


                            
            if self.train and self.embeddings:
                try:
                    embedding,logits = embed_sample(self.embedding_model, x.numpy(), SAMPLE_RATE)
                    #birdnet_embedding = np.expand_dims(outputs, axis=0)
                except:
                    print("BirdNET embedding failed")
                    birdnet_embedding = np.zeros(320)  

            #Resample the audio from 48k to 16k for AST

            resampler = T.Resample(SAMPLE_RATE, SAMPLE_RATE_AST, dtype=x.dtype)
            x = resampler(x)            

                
            y = torch.tensor(int(self.labels[sample_path]), dtype=torch.long)
            y = torch.nn.functional.one_hot(y, self.num_classes).float()  
            

        if self.train:    

            
            #small_scores = logits[self.indices]
            #return {'inputs': x, 'samples': sample_path, 'targets': small_scores}, y 
            
            #return {'inputs': x, 'samples': sample_path}, y
            return {'inputs': x, 'samples': sample_path, 'emb': embedding}, y
        else:

            return {'inputs': x, 'samples': sample_path}, y
            #return {'inputs': x, 'samples': sample_path, 'emb': embedding}, y
      

    def replace_XC_Birdnet(self, species_list):
        
        # Read the CSV file into a pandas DataFrame
        df = pd.read_csv('/home/ubuntu/burooj/AvesEcho/checkpoints/output_species.csv')

        # Loop through the list of species names
        for i in range(len(species_list)):
            # Add a space before every uppercase letter (except the first letter)
            #modified_name = ''.join([' '+c if c.isupper() and i!=0 else c for i,c in enumerate(preds[i])]).lstrip()
            # Add a space before every uppercase letter (except the first letter and uppercase letters preceded by a hyphen)
            modified_name = ''.join([' ' + c if c.isupper() and j != 0 and species_list[i][j - 1] != '-' else c for j, c in enumerate(species_list[i])]).lstrip()
            # Look up the part after underscore in the second column of the DataFrame
            match = df[df['XC'].apply(lambda x: x.split('_')[1].replace('\'',''))==modified_name]
            # If there is a match, extract the part after underscore in the first column and replace the element in the list
            if not match.empty:
                code = match.iloc[0]['ebird'].split('_')[1].replace(' ','')
                species_list[i] = code
        return species_list               


class InferenceDataset(torch.utils.data.Dataset):
    def __init__(self, list_IDs, NumClasses):
        # Initialization
        self.list_IDs = list_IDs
        self.NumClasses = NumClasses
        self.embedding_model = BirdNet(48000, 'checkpoints/BirdNET_GLOBAL_3K_V2.2_Model_FP32.tflite')

    def __len__(self):
        # Denotes the total number of samples
        return len(self.list_IDs)

    def __getitem__(self, index):
        # Generates one sample of data
        ID = self.list_IDs[index]

        # Open the file with librosa (limited to the first certain number of seconds)
        try:
            x, rate = librosa.load(ID, sr=SAMPLE_RATE, offset=0.0, res_type='kaiser_fast')
        except:
            x, rate = [], SAMPLE_RATE   

        x = x - x.mean()

        #convert mixed to tensor
        x = torch.from_numpy(x).float() 

        #Resample the audio from 48k to 32k for PaSST

        resampler = T.Resample(SAMPLE_RATE, SAMPLE_RATE_AST, dtype=x.dtype)
        x = resampler(x)      

       
        # Compute BirdNET embedding
       
        try:
            outputs, logits = embed_sample(self.embedding_model, x.numpy(),SAMPLE_RATE)
            birdnet_embedding = np.expand_dims(outputs, axis=0)
        except:
            print("BirdNET embedding failed")
            birdnet_embedding = np.zeros(320)  

         
        return {'inputs': x, 'emb': birdnet_embedding, 'file': ID} 
