# -*- coding: utf-8 -*-
# @Time    : 29/08/23 9:00 
# @Author  : Burooj Ghani
# @Affiliation  : Naturalis Biodiversity Center
# @Email   : burooj.ghani at naturalis.nl
# @File    : analyse.py


import torch
import torch.nn as nn
from dataset import *
from root_modules import *
from models import *
from train_val import *
from util import *
from config import *
import glob 
import csv
import argparse
import json

from IPython.display import Audio

from hear21passt.base import get_basic_model, get_model_passt

train_on_gpu=torch.cuda.is_available()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


# I/O args
parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--slist', type=str, default='inputs/list_gt_9', help='list of species')
parser.add_argument('--flist', type=str, default='inputs/species_list_nl.csv', help='filter by a list of species')
parser.add_argument('--i', type=str, default='audio/', help='input dir')
parser.add_argument('--o', type=str, default='tmp/avesecho', help='output directory')
parser.add_argument('--mconf', type=float, default=0.0, help='minimum confidence, defaults to 0.2')
parser.add_argument('--add_filtering', action='store_true', help='location filtering')
parser.add_argument('--add_csv', action='store_true', help='save a list of predictions as a csv')

args = parser.parse_args()


# Set the species list
species_list_path = args.slist
species_list = []
with open(species_list_path) as f:
    for line in f:
        species_list.append(line.strip())
species_list = sorted(species_list)   
NumClasses = len(species_list)  


# Load the model
model = get_basic_model(mode = 'logits', arch="passt_s_kd_p16_128_ap486")
model.net =  get_model_passt(arch="passt_s_kd_p16_128_ap486",  n_classes=NumClasses)
model.load_state_dict(torch.load('checkpoints/best_model_passt_439_Aug_KD_3000.pt', map_location=device))
model = model.to(device)


def create_filtering_list(flist, df_b):
    # Load CSV files into dataframes
    df_a = pd.read_csv(flist, header=None)
    
    # Create an empty list to store results
    results_list = []

    # Iterate through each row in df_a
    for index, row in df_a.iterrows():
        prediction = row[0]
        if "_" in prediction:
            prediction = prediction.split('_')[0]
        try:
            # Apply the specified operation and store the result in df_c
            result = df_b[df_b['XC'].apply(lambda x: x.split('_')[0]) == prediction].iloc[0]['XC'].split('_')[1].replace(' ', '').replace('\'','')
            results_list.append([result])
        except IndexError:
            # Handle case where no matching row is found in df_b
            #print(f"No matching row found in df_b for prediction: {prediction}")
            continue

    # Create df_c from results_list
    df_c = pd.DataFrame(results_list, columns=['Result'])

    # Optionally, reset the index of df_c
    df_c.reset_index(drop=True, inplace=True)

    df_c.to_csv('inputs/species_list.csv', index=False, header=None)

for filename in os.listdir(args.i):

    if not (filename.endswith('.wav') or filename.endswith('.mp3') or filename.endswith('.WAV')):
        continue  # Skip the rest of the loop for this iteration


    files = glob.glob(os.path.join(args.o, '*'))

    # Load parameters
    sound = os.path.join(args.i, filename)
    flist = args.flist
    m_conf = args.mconf

    if not os.path.exists(args.o):
        os.makedirs(args.o)

    # Split signal into 3s chunks
    split_signals(sound, args.o, 3)


    # Extract the filename from the path
    filename = sound.split('/')[-1]  # This splits the string by '/' and gets the last element
    filename_without_ext = filename.split('.')[0]  # This splits the filename by '.' and gets the first element

    # Create a predictions file name
    pred_name = 'outputs/predictions_' + filename_without_ext + '.csv'


    #Load a list of files for in a dir
    inference_dir = args.o
    inference_data = [os.path.join(inference_dir, f) for f in sorted(os.listdir(inference_dir), key=lambda x: int(x.split('_')[-1].split('.')[0]))]

    #Inference
    inference_set = InferenceDataset(inference_data, NumClasses)
    params_inf = {'batch_size': 64, 'shuffle': False, 'num_workers': 6}
    inference_generator = torch.utils.data.DataLoader(inference_set, **params_inf)

    # Maps species common names to scientific names and also across XC and eBird standards and codes
    df = pd.read_csv('inputs/species_mapping.csv')

    create_filtering_list(flist, df_b = df)

    predictions, predictions_filtered, scores_filtered, scores, files, scores_all, species_names_all = inference(model, inference_generator, device, species_list, 'inputs/species_list.csv')

    names = []

    for i, prediction in enumerate(species_names_all):
        modified_name = ''.join([' ' + c if c.isupper() and j != 0 and prediction[j - 1] != '-' else c for j, c in enumerate(prediction)]).lstrip()
        match = df[df['XC'].apply(lambda x: x.split('_')[1].replace('\'',''))==modified_name]
        sc_name = match.iloc[0]['XC']
        names.append(sc_name)


    # The name of your CSV file
    filename = 'species_nl.csv'

    # Writing the list to the CSV file
    with open(filename, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        for item in names:
            csvwriter.writerow([item])

    def create_json_output(predictions, scores, files, args, df, add_csv, filtered=False):


        # Creating headers
        headers = ['Begin Time', 'End Time', 'File'] + [f'Species {i+1}' for i in range(scores_all.shape[1])]

        if add_csv:
            with open(pred_name, 'w', newline='') as file:
                writer = csv.writer(file)
                #writer.writerow(["Begin Time", "End Time", "File", "Prediction", "Score"])  # write header
                writer.writerow(headers)




        output = {
            "$comment": "based on https://docs.google.com/document/d/1xliXgmgBj0vu_E2M-3tu-VJWSk_rN2rQt4-XSBCTkxg/edit and then updated 2022-04 to bring into sync with camtrap-DP developments",
            "generated_by": {
                "datetime": "2021-04-14T13:26:29Z",
                "tag": "mfn_euro_birds",
                "version": "1fd68f8c8cb93ec4e45049fcf9a056628e9599aa815790a2a7b568aa"
            },
            "media": [{"filename": args.i, "id": args.i.split('/')[-1]}],
            "region_groups": [],
            "predictions": []
        }

        for i, prediction in enumerate(predictions):
            begin_time = i * 3
            end_time = begin_time + 3
            
            # Set a threshold for scores, 0.1 for unfiltered and 0.2 for filtered
            threshold = m_conf

            modified_name = ''.join([' ' + c if c.isupper() and j != 0 and prediction[j - 1] != '-' else c for j, c in enumerate(prediction)]).lstrip()
            match = df[df['XC'].apply(lambda x: x.split('_')[1].replace('\'',''))==modified_name]
            sc_name = match.iloc[0]['XC']
            code =  match.iloc[0]['code']
            if scores[i] > threshold:
                region_group_id = f"{files[i]}?region={i}"
                if add_csv:
                    with open(pred_name, 'a', newline='') as file:
                        writer = csv.writer(file)
                        writer.writerow([begin_time, end_time, files[i]] + scores_all[i].tolist())
                        #writer.writerow([begin_time, end_time, files[i], sc_name, scores[i]])
                output["region_groups"].append({
                    "id": region_group_id,
                    "regions": [{
                        "media_id": args.i.split('/')[-1],
                        "box": {
                            "t1": float(begin_time),
                            "t2": float(end_time)
                        }
                    }]
                })
                
                output["predictions"].append({
                    "region_group_id": region_group_id,
                    "taxa": {
                        "type": "multiclass",
                        "items": [{
                            "scientific_name": sc_name,
                            "probability": scores[i],
                            "taxon_id": code  # Fill in taxon_id if available
                        }]
                    }
                })
        
        # Determine the output file name based on filtering
        json_name = f'outputs/predictions_filtered_{sound.split("/")[-1].split(".")[0]}.json' if filtered else f'outputs/predictions_{sound.split("/")[-1].split(".")[0]}.json'
        
        # Write the output dictionary to a JSON file
        with open(json_name, 'w') as json_file:
            json.dump(output, json_file, indent=4)





    if args.add_filtering == False:
        create_json_output(predictions, scores, files, args, df, args.add_csv)
    else:
        create_json_output(predictions_filtered, scores_filtered, files, args, df, args.add_csv, filtered=True)


    # Empty temporary audio chunks
    for f in files:
        os.remove(f)

  




